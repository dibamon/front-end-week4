import React from 'react';
import './App.css';
import TimeDisplay from './TimeDisplay'

class App extends React.Component {
  state = { time: new Date().toLocaleTimeString([], { hour12: true, hourCycle: "h12" , hour:"2-digit",minute:"2-digit",second:"2-digit" } ) };

  componentDidMount(){

    setInterval(() => { this.setState({ time: new Date().toLocaleTimeString([], { hour12: true, hourCycle:"h12",hour:"2-digit",minute:"2-digit",second:"2-digit" } )})},1000)

  }
  
  render(){
  return <TimeDisplay time = {this.state.time}/>
  }
}

export default App;
