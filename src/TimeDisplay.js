import React from 'react';
import './TimeDisplay.css';

const getTime = (time,Typography)=>{
    var res = time.substring(0, 2);
    if(res >= 6 && res <= 11 && Typography==='AM'){
        return 'day';
    }else if ( (res <= 7 || res === '12') && Typography==='PM' ){
        return 'day';
    } else if (res >= 8 && res <= 11 && Typography === 'PM'){
        return 'night';
    } else if ( (res <= 5 || res==='12')&& Typography === 'AM'){
        return 'night';
    }
}


const TimeDisplay = (props) =>{

    const Typography = props.time.substring(props.time.length - 2, props.time.length);
    console.log(Typography);
    const dayNight = getTime(props.time,Typography);
    
    const icone = dayNight==='day'? 'sun': 'moon' ;
    
    return (
        <div className={`time-display ${dayNight}`}> 
             <i className={`Left massive ${icone} icon `}/>
             <h1 className='text' >{props.time}</h1>
        </div>
        );

};

export default TimeDisplay ;